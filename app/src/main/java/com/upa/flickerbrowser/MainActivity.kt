package com.upa.flickerbrowser

import android.net.Uri
import android.nfc.NdefRecord.createUri
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import java.lang.Exception
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), GetRawData.onDownloadComplete, GetFlickrJsonData.OnDataAvailable {
    //GetRawData.onDownloadComplete pridat az potom, co vytvorim interface v tride GetRawData
    private val TAG = "MainActivity"

    private val flickrRecyclerViewAdapter = FlickrRecyclerViewAdapter(ArrayList())

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate called")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        //tohle udelat az potom, co bude hotovy Adapter
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = flickrRecyclerViewAdapter

        //tohle udělat až po tříde GetRawData
        //val getRawData = GetRawData()
        val url = createUri("https://api.flickr.com/services/feeds/photos_public.gne", "android, oreo", "en-us", true)
        val getRawData =GetRawData(this)


        //getRawData.setDownloadCompleteListener(this)
        //getRawData.execute("https://api.flickr.com/services/feeds/photos_public.gne?tags=android&format=json&nojsoncallback=1")
        getRawData.execute(url)
        //níže uvedený kód nebudeme zatím potřebovat
       /* findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/
        Log.d(TAG, "onCreate ends")
    }

    private fun createUri(baseURL: String, searchCriteria: String, lang: String, matchAll: Boolean):String{
        Log.d(TAG, ".createUri starts")
        //tvorime URI

        var uri = Uri.parse(baseURL)
        var builder = uri.buildUpon()
        builder = builder.appendQueryParameter("tags", searchCriteria)
        builder = builder.appendQueryParameter("tagmode", if (matchAll) "ALL" else "ANY")
        builder = builder.appendQueryParameter("lang", lang)
        builder = builder.appendQueryParameter("format", "json")
        builder = builder.appendQueryParameter("nojsoncallback", "1")
        uri = builder.build()
        return uri.toString()

//        var uri = Uri.parse(baseURL)
//            .buildUpon()
//            .appendQueryParameter("tags", searchCriteria)
//            .appendQueryParameter("tagmode", if (matchAll) "ALL" else "ANY")
//            .appendQueryParameter("lang", lang)
//            .appendQueryParameter("format", "json")
//            .appendQueryParameter("nojsoncallback", "1")
//            .build()

//        return Uri.parse(baseURL)
//            .buildUpon()
//            .appendQueryParameter("tags", searchCriteria)
//            .appendQueryParameter("tagmode", if (matchAll) "ALL" else "ANY")
//            .appendQueryParameter("lang", lang)
//            .appendQueryParameter("format", "json")
//            .appendQueryParameter("nojsoncallback", "1")
//            .build()
//            .toString()


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.d(TAG, "onCreateOptionsMenu")
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Log.d(TAG, "onOptionsItemSelected")
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

//    jen ukazat - pak nastavit globalni promenou na zacatku
//    companion object {
//        private const val TAG = "MainActivity"
//    }
    // jak se prida interface, bude tady chyba...pridat override
    // override - prepisuje funkci, ktera je v superclass - to je ten interface
    override fun onDownloadComplete(data: String, status: DownloadStatus){
        if (status == DownloadStatus.OK){
            Log.d(TAG, "onDownloadComplete called, data is $data")

            val getFlickrJsonData = GetFlickrJsonData(this)
            getFlickrJsonData.execute(data)
        } else {
            Log.d(TAG, "onDownloadComplete failed with status $status. Error message is: $data")
        }
    }

    //až bude hotova trida GetFlickrJsonData
    override fun onDataAvailable(data: List<Photo>) {
        Log.d(TAG, ".onDataAvailable called, data is $data")
        //tohle udelat az potom, co bude hotovy Adapter
        flickrRecyclerViewAdapter.loadNewData(data)

        Log.d(TAG, ".onDataAvailable ends")

    }

    override fun onError(exception: Exception) {
        Log.d(TAG, ".onError called with ${exception.message}")
    }
}