package com.upa.flickerbrowser


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class FlickrImageViewHolder(view: View) : RecyclerView.ViewHolder(view){
    //tuto tridu vytvorit az potom, co vytvorim tridu FlickrRecyclerViewAdapter
    //tato trida drzi vlastnosti k jednotlivym prvkum
    //musime implementovat ViewHolder
    var thumbnail: ImageView = view.findViewById(R.id.thumbnail)
    var title: TextView = view.findViewById(R.id.title)
}
class FlickrRecyclerViewAdapter(private var photoList: List<Photo>): RecyclerView.Adapter<FlickrImageViewHolder>() {
    //bude chyba
    //musime implementovat metody / Ctrl+I

    private val TAG = "FlickrRecyclerViewAdapt" //ne delsi nez 23 znaku

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlickrImageViewHolder {
        //Called by layout manager when it needs a new view
        Log.d(TAG, ".onCreateViewHolder new view requested")

        val view = LayoutInflater.from(parent.context).inflate(R.layout.browse, parent, false)
        return FlickrImageViewHolder(view)
        //inflate xml soubor tim, cim to chceme naplnit (nafouknout)
    }

    override fun onBindViewHolder(holder: FlickrImageViewHolder, position: Int) {
        //called by layout manager when it wants new data in existing view
        val photoItem = photoList[position]
        Log.d(TAG, ".onBindViewHolder: ${photoItem.title} --> $position")
        Picasso.get().load(photoItem.image)
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(holder.thumbnail)
        holder.title.text = photoItem.title
    }

    override fun getItemCount(): Int {
        Log.d(TAG, ".getItemCount")
        return if (photoList.isNotEmpty()) photoList.size else 0

    }

    fun loadNewData(newPhotos: List<Photo>){
        photoList = newPhotos
        notifyDataSetChanged()
    }

    fun getPhoto(position: Int): Photo?{
        //pokud chceme provest nejakou akci - kliknout na fotku
        return if (photoList.isNotEmpty()) photoList[position] else null
    }
}